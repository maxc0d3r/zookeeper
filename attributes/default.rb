cookbook_name = 'zookeeper'

default[cookbook_name]['version'] = '3.4.10'
default[cookbook_name]['checksum'] = '7f7f5414e044ac11fee2a1e0bc225469f51fb0cdf821e67df762a43098223f27'
default[cookbook_name]['mirror'] = 'http://archive.apache.org/dist/zookeeper/'
default[cookbook_name]['user'] = 'zookeeper'
default[cookbook_name]['prefix_root'] = '/opt'
default[cookbook_name]['prefix_home'] = '/opt'
default[cookbook_name]['prefix_bin'] = '/opt/bin'
default[cookbook_name]['log_dir'] = '/var/log/zookeeper'
default[cookbook_name]['data_dir'] = '/var/lib/zookeeper'
default[cookbook_name]['java'] = {
  'centos' => 'java-1.8.0-openjdk-headless'
}
default[cookbook_name]['auto_restart'] = true
default[cookbook_name]['unit_path'] = '/etc/systemd/system'
default[cookbook_name]['role'] = cookbook_name
default[cookbook_name]['hosts'] = []
default[cookbook_name]['size'] = 3
default[cookbook_name]['config'] = {
  'clientPort' => 2181,
  'dataDir' => node[cookbook_name]['data_dir'],
  'tickTime' => 2000,
  'initLimit' => 5,
  'syncLimit' => 2
}
default[cookbook_name]['jvm_opts'] = {
  '-Dcom.sun.management.jmxremote' => nil,
  '-Dcom.sun.management.jmxremote.authenticate' => false,
  '-Dcom.sun.management.jmxremote.ssl' => false,
  '-Dcom.sun.management.jmxremote.port' => 2191,
  '-Djava.rmi.server.hostname' => node['fqdn']
}
default[cookbook_name]['log4j'] = {
  'log4j.rootLogger' => 'INFO, ROLLINGFILE',
  'log4j.appender.CONSOLE' => 'org.apache.log4j.ConsoleAppender',
  'log4j.appender.CONSOLE.Threshold' => 'INFO',
  'log4j.appender.CONSOLE.layout' => 'org.apache.log4j.PatternLayout',
  'log4j.appender.CONSOLE.layout.ConversionPattern' =>
    '%d{ISO8601} [myid:%X{myid}] - %-5p [%t:%C{1}@%L] - %m%n',
  'log4j.appender.ROLLINGFILE' => 'org.apache.log4j.RollingFileAppender',
  'log4j.appender.ROLLINGFILE.Threshold' => 'INFO',
  'log4j.appender.ROLLINGFILE.File' =>
    "#{node[cookbook_name]['log_dir']}/zookeeper.log",
  'log4j.appender.ROLLINGFILE.MaxFileSize' => '10MB',
  'log4j.appender.ROLLINGFILE.layout' => 'org.apache.log4j.PatternLayout',
  'log4j.appender.ROLLINGFILE.layout.ConversionPattern' =>
    '%d{ISO8601} [myid:%X{myid}] - %-5p [%t:%C{1}@%L] - %m%n',
  'log4j.appender.TRACEFILE' => 'org.apache.log4j.FileAppender',
  'log4j.appender.TRACEFILE.Threshold' => 'TRACE',
  'log4j.appender.TRACEFILE.File' =>
    "#{node[cookbook_name]['log_dir']}/zookeeper_trace.log",
  'log4j.appender.TRACEFILE.layout' => 'org.apache.log4j.PatternLayout',
  'log4j.appender.TRACEFILE.layout.ConversionPattern' =>
    '%d{ISO8601} [myid:%X{myid}] - %-5p [%t:%C{1}@%L][%x] - %m%n'
}
