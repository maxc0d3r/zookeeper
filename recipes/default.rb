#
# Cookbook:: zookeeper
# Recipe:: default
#
# Copyright:: 2017, Kashif, All Rights Reserved.

include_recipe "#{cookbook_name}::set_non_nat_vbox_ip"
include_recipe "#{cookbook_name}::install"
include_recipe "#{cookbook_name}::user"
include_recipe "#{cookbook_name}::config"
include_recipe "#{cookbook_name}::service"
