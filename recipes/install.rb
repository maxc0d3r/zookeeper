package 'tar'

version = node[cookbook_name]['version']
zookeeper_url = "#{node[cookbook_name]['mirror']}/zookeeper-#{version}"
zookeeper_artifact = "zookeeper-#{version}.tar.gz"

[
  node[cookbook_name]['prefix_root'],
  node[cookbook_name]['prefix_home'],
  node[cookbook_name]['prefix_bin']
].uniq.each do |dir_path|
  directory "zookeeper:#{dir_path}" do
    path dir_path
    owner 'root'
    group 'root'
    mode '0755'
    recursive true
    action :create
  end
end

ark 'zookeeper' do
  action :install
  url "#{zookeeper_url}/#{zookeeper_artifact}"
  prefix_root node[cookbook_name]['prefix_root']
  prefix_home node[cookbook_name]['prefix_home']
  prefix_bin node[cookbook_name]['prefix_bin']
  has_binaries [] # zookeeper script does not work when linked
  checksum node[cookbook_name]['checksum']
  version node[cookbook_name]['version']
end

install_dir = "#{node[cookbook_name]['prefix_home']}/zookeeper"

link "#{install_dir}/zookeeper.jar" do
  to "#{install_dir}/zookeeper-#{version}.jar"
end
