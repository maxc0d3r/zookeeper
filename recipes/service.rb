node.run_state[cookbook_name] ||= {}
return if node.run_state[cookbook_name]['abort?']

config_path = "#{node[cookbook_name]['prefix_home']}/zookeeper/conf"
install_path = "#{node[cookbook_name]['prefix_home']}/zookeeper"

service_config = {
  classpath: "#{install_path}/zookeeper.jar:#{install_path}/lib/*",
  config_file: "#{config_path}/zoo.cfg",
  log4j_file: "#{config_path}/log4j.properties"
}

execute 'zookeeper-platform:systemd-reload' do
  command 'systemctl daemon-reload'
  action :nothing
end

unit_file = "#{node[cookbook_name]['unit_path']}/zookeeper.service"
template unit_file do
  variables service_config
  mode '0644'
  source 'zookeeper.service.erb'
  notifies :run, 'execute[zookeeper-platform:systemd-reload]', :immediately
end

java = node[cookbook_name]['java']

unless java.to_s.empty?
  java_package = java[node['platform']]

  if java_package.to_s.empty?
    Chef::Log.warn  "No java specified for the platform #{node['platform']}, "\
                    'java will not be installed'

    Chef::Log.warn  'Please specify a java package name if you want to '\
                    'install java using this cookbook.'
  else
    package java_package
  end
end

config_files = [
  "#{config_path}/zoo.cfg",
  "#{config_path}/log4j.properties",
  "#{node[cookbook_name]['data_dir']}/myid"
].map do |path|
  "template[#{path}]"
end

auto_restart = node[cookbook_name]['auto_restart']

service 'zookeeper' do
  provider Chef::Provider::Service::Systemd
  supports status: true, restart: true, reload: true
  action %i[enable start]
  subscribes :restart, config_files if auto_restart
end
