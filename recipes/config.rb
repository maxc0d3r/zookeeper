::Chef::Recipe.send(:include, ClusterSearch)
cluster = cluster_search(node[cookbook_name])
node.run_state[cookbook_name] ||= {}
if cluster.nil?
  node.run_state[cookbook_name]['abort?'] = true
  return
end

config = node[cookbook_name]['config'].dup
cluster['hosts'].each_with_index do |v, i|
  config["server.#{i + 1}"] = "#{v}:2888:3888"
end

[
  node[cookbook_name]['log_dir'],
  node[cookbook_name]['data_dir']
].each do |path|
  directory path do
    owner node[cookbook_name]['user']
    group node[cookbook_name]['user']
    mode '0755'
    recursive true
    action :create
  end
end

config_path = "#{node[cookbook_name]['prefix_home']}/zookeeper/conf"

template "#{config_path}/zoo.cfg" do
  variables config: config
  mode '0644'
  source 'zoo.cfg.erb'
end

template "#{config_path}/log4j.properties" do
  source 'properties.erb'
  mode '644'
  variables config: node[cookbook_name]['log4j']
end

template "#{node[cookbook_name]['data_dir']}/myid" do
  variables my_id: cluster['my_id']
  mode '0644'
  source 'myid.erb'
end
