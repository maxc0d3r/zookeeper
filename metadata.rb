name 'zookeeper'
maintainer 'Kashif'
maintainer_email 'devops.kashif@gmail.com'
license 'all_rights'
description 'Installs/Configures zookeeper'
long_description 'Installs/Configures zookeeper'
version '0.1.0'

chef_version '>= 12.0'

supports 'centos', '>= 7.1'

depends 'ark'
depends 'cluster-search'
